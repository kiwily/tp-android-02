package com.example.tp02_2;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

class AsyncBitmapDownloader extends AsyncTask<String, Void, Bitmap> {

    private final WeakReference<ImageView> imageReference;

    public AsyncBitmapDownloader(ImageView imageView) {
        imageReference = new WeakReference(imageView);
    }

    @Override
    protected Bitmap doInBackground(String... urlString) {
        URL url2 = null;
        try {
            url2 = new URL(urlString[0]);
            HttpsURLConnection urlConnection = (HttpsURLConnection) url2.openConnection();

            try {
                InputStream in = new BufferedInputStream((urlConnection.getInputStream()));
                Log.i("JFL", "url: " + url2);
                Bitmap bm = BitmapFactory.decodeStream(in);
                Log.i("JFL", "is null ? " + ((Boolean) (bm == null)).toString());
                return bm;
            } finally {
                urlConnection.disconnect();
            }
        } catch (MalformedURLException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    @Override
    protected void onPostExecute(Bitmap bm) {
        if (imageReference != null ) {
            if (bm != null) {
                Log.i("JFL", "received non null");
                imageReference.get().setImageBitmap(bm);
            }
        }
    }
}