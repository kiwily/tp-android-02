package com.example.tp02_2;

import android.os.AsyncTask;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

class AsyncFlickrJSONData extends AsyncTask<String, Void, JSONObject> {

    private final ImageView imageView;

    public AsyncFlickrJSONData(ImageView img) {
        imageView = img;
    }

    @Override
    protected JSONObject doInBackground(String... urlString) {
        URL url = null;
        try {
            url = new URL(urlString[0]);
            HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();

            try {
                InputStream in = new BufferedInputStream((urlConnection.getInputStream()));
                String s = readStream(in);
                s = String.valueOf(s.subSequence(15, s.length() - 1));
                try {
                    JSONObject j = new JSONObject(s);
                    return j;
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } finally {
                urlConnection.disconnect();
            }
        } catch (MalformedURLException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new JSONObject();
    }

    @Override
    protected void onPostExecute(JSONObject json) {
        try {
            JSONArray items = json.getJSONArray("items");
            int i = (int) (Math.random() * items.length());
            JSONObject object = items.getJSONObject(i);
            JSONObject media = object.getJSONObject("media");
            String link = media.getString("m");
            new AsyncBitmapDownloader(imageView).execute(link);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String readStream(InputStream is) {
        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            int i = is.read();
            while (i != -1) {
                bo.write(i);
                i = is.read();
            }
            return bo.toString();
        } catch (IOException e) {
            return "";
        }
    }
}
