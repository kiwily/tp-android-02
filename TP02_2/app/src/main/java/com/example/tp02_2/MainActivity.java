package com.example.tp02_2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    Button getImage = null;
    Button goToList = null;
    ImageView imageView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getImage = findViewById(R.id.getImageButton);
        goToList = findViewById(R.id.goToListButton);
        imageView = findViewById(R.id.image);

        getImage.setOnClickListener(new GetImageOnClickListener(imageView));
        goToList.setOnClickListener((v) -> {
            Intent intent = new Intent(this, ListActivity.class);
            startActivity(intent);
        });

    }
}

class GetImageOnClickListener implements View.OnClickListener {

    private final ImageView imageView;

    public GetImageOnClickListener(ImageView img) {
        imageView = img;
    }
    @Override
    public void onClick(View v) {
        Log.i("JFL", "click" + imageView.getId());
        new AsyncFlickrJSONData(imageView).execute(
                "https://www.flickr.com/services/feeds/photos_public.gne?tags=cats&format=json");
    }
}
