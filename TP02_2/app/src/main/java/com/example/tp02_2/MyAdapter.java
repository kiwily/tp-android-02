package com.example.tp02_2;


import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;

import java.util.Vector;

class MyAdapter extends BaseAdapter {

    Vector<String> vector = new Vector<String>();
    Context context = null;
    RequestQueue queue = null;

    public MyAdapter  (Context ctx) {
        context = ctx;
        queue = MySingleton.getInstance(context).getRequestQueue();
    }
    @Override
    public int getCount() {
        if (vector != null){
            return vector.size();
        }
        return 0;
    }

    @Override
    public String getItem(int i) {
        if (vector != null){
            return vector.get(i);
        }
        return "";
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        /*
        // Display url only
        if (view == null){
            view = LayoutInflater.from(context).inflate(R.layout.textviewlayout, viewGroup, false);
        }
        TextView textView = view.findViewById(R.id.textView);
        textView.setText(getItem(i));
        return textView;*/

        if (view == null){
            view = LayoutInflater.from(context).inflate(R.layout.bitmaplayout, viewGroup, false);
        }
        ImageView imageView = view.findViewById(R.id.imageView);
        ImageRequest imageRequest = new ImageRequest(getItem(i), new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                imageView.setImageBitmap(response);
            }
        }, 0, 0, ImageView.ScaleType.CENTER_CROP, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Error when downloading image", Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        });
        MySingleton.getInstance(context).addToRequestQueue(imageRequest);
        return imageView;
    }

    public void add(String url){
        vector.add(url);
    }
}