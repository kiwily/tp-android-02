package com.example.tp02_2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;
import java.util.zip.Inflater;

import javax.net.ssl.HttpsURLConnection;

public class ListActivity extends AppCompatActivity {

    ListView liste = null;
    LocationManager locationManager = null;
    LocationListener locationListener = null;
    MyAdapter adapter = null;
    String lon = "0";
    String lat = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        // Set up our display list
        liste = findViewById(R.id.list);
        adapter = new MyAdapter(this);
        liste.setAdapter(adapter);

        // Ask permissions
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001);

            return;
        }

        // Get GPS coordinates
        locationListener = location -> {
            lon = String.valueOf(location.getLongitude());
            lat = String.valueOf(location.getLatitude());
            Log.i("JFL", String.valueOf(location.getLatitude()) + String.valueOf(location.getLongitude()));
        };
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestSingleUpdate(criteria, locationListener, null);

        // Fetch geolocalized data
        // TODO use a real api_key...
        new AsyncFlickrJSONDataForList(adapter).execute(
                "https://www.flickr.com/services/feeds/photos_public.gne?tags=cats&format=json"
                /*"https://www.flickr.com/services/rest/?" +
                        "method=flickr.photos.search" +
                        "&license=4" +
                        "&api_key=xxxxxxxxxx" +
                        "&has_geo=1&lat=" + lat +
                        "&lon=" + lon + "&per_page=1&format=json"*/);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] perm, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, perm, grantResults);
        if (requestCode == 1001){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)){

                Log.i("JFL", "et iciiii?" + locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));

            }
        }
    }
}