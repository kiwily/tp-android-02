package com.example.tp01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class Acheter extends AppCompatActivity {

    TextView afficheAcheter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acheter);
        Intent intent = getIntent();
        String achat = intent.getStringExtra(MainActivity.ACHAT);

        afficheAcheter = (TextView) findViewById(R.id.afficheAcheter);

        afficheAcheter.setText("Voulez-vous acheter " + achat + "?");
    }
}