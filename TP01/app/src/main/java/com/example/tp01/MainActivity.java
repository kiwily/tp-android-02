package com.example.tp01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;

public class MainActivity extends AppCompatActivity {

    EditText userLogin = null;
    TimePicker time = null;
    Button addUser = null;
    Button quit = null;
    Button faireCourses = null;
    TextView textResults = null;
    static String EXTRA_MESSAGE = "test";
    static String ACHAT = "achat";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_layout);

        addUser = (Button) findViewById(R.id.addUser);
        quit = (Button) findViewById(R.id.quitter);
        faireCourses = (Button) findViewById(R.id.faireCourses);
        userLogin = (EditText) findViewById(R.id.userLogin);
        textResults = (TextView) findViewById(R.id.resultText);
        time = (TimePicker) findViewById(R.id.timePicker);

        addUser.setOnClickListener((v) -> {
            textResults.setText(
                    userLogin.getText() +
                            " doit faire les courses à " +
                            time.getHour() + ":" +
                            time.getMinute());
            userLogin.clearComposingText();
        });
        faireCourses.setOnClickListener((v) -> {
            Intent intent = new Intent(this, ListeActivity.class);
            String message = userLogin.getText().toString();
            intent.putExtra(EXTRA_MESSAGE, message);
            startActivity(intent);
        });
        quit.setOnClickListener((v) -> {
            System.exit(0);
        });
    }
}