package com.example.tp01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ArrayList;

public class ListeActivity extends AppCompatActivity {

    TextView afficheLogin = null;
    ListView liste = null;
    String[] values = new String[] {"oeuf", "jambon", "champignons"};
    ArrayList<String> listStrings =  new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste);
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        afficheLogin = (TextView) findViewById(R.id.afficheLogin);
        liste = (ListView) findViewById(R.id.liste);

        afficheLogin.setText(message);

        for (int i = 0; i < values.length; ++i){
            listStrings.add(values[i]);
        }
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);
        liste.setAdapter(adapter);

        liste.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), Acheter.class);
                String message = adapter.getItem(i);
                intent.putExtra(MainActivity.ACHAT, message);
                startActivity(intent);
            }
        });

    }
}