package com.example.tp02_1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    static EditText loginText = null;
    static EditText passwordText = null;
    static TextView resultText = null;
    Button authButton = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loginText = findViewById(R.id.loginText);
        passwordText = findViewById(R.id.passwordText);
        resultText = findViewById(R.id.resultText);
        authButton = findViewById(R.id.authButton);

        authButton.setOnClickListener((v) -> {
            new Thread(new Runnable() {
                @Override
                public void run(){
                    URL url = null;
                    try {
                        url = new URL("https://httpbin.org/basic-auth/login/password");

                        HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();

                        String auth = loginText.getText().toString() + ":" + passwordText.getText().toString();
                        String basicAuth = "Basic " + Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
                        urlConnection.setRequestProperty("Authorization", basicAuth);
                        try {
                            Log.i("JFL", String.valueOf(urlConnection.getResponseCode()));
                            InputStream in = new BufferedInputStream((urlConnection.getInputStream()));
                            String s = readStream(in);
                            try {
                                JSONObject j = new JSONObject(s);
                                Boolean authed = j.getBoolean("authenticated");
                                runOnUiThread (new Runnable() {
                                    @Override
                                    public void run() {
                                        resultText.setText(authed.toString());
                                        Log.i("JFL", authed.toString());
                                    }
                                });
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.i("JFL", s);

                        } finally {
                            urlConnection.disconnect();
                        }
                    } catch (MalformedURLException e){
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                private String readStream(InputStream is) {
                    try {
                        ByteArrayOutputStream bo = new ByteArrayOutputStream();
                        int i = is.read();
                        while (i != -1) {
                            bo.write(i);
                            i = is.read();
                        }
                        return bo.toString();
                    } catch (IOException e) {
                        return "";
                    }
                }
            }).start();
        });
    }
}